#include <stdlib.h>
#include <stdio.h>

#include "mylist.h"

void mylist_init(struct mylist* l)
{
  l->head = NULL;
}

void mylist_destroy(struct mylist* l)
{
  struct mylist_node *tmp=NULL;
  struct mylist_node *p=l->head;

  while(1){
    if(p==NULL){
      break;
    }
    tmp=p;
    p=p->next;
    free(tmp);
  } 

}

void mylist_insert(
    struct mylist* l,
    struct mylist_node* before, int data)
{
  struct mylist_node *node=(struct mylist_node *)malloc(sizeof(struct mylist_node));
  if(before==NULL){
    node->data = data;
    node->next=l->head;
    l->head = node;
  }
  else{
    node->data = data;
    node->next=before->next;
    before->next=node;
  }
}

void mylist_remove(
    struct mylist* l,
    struct mylist_node* target)
{
  if(target==NULL){
    return;
  }
  struct mylist_node *t;
  struct mylist_node *b;
  b=NULL;
  if(l->head==NULL){
    return;
  }
  for(t=l->head;t!=NULL;t=t->next){
    if(t==target){
      if(b==NULL){
        l->head=t->next;
        free(t);
        break;
      }
      b->next=t->next;
      free(t);
      break;
    }
    b=t;
  }
}

struct mylist_node* mylist_find(struct mylist* l, int target)
{
  for(struct mylist_node *p=l->head;p!=NULL;p=p->next){
    if(p->data==target){
      return p;
    }
  }
  return NULL; // If not found
}

struct mylist_node* mylist_get_head(struct mylist* l)
{
  return l->head;
}

void mylist_print(const struct mylist* l)
{
  
  for (struct mylist_node* pointer = l->head;
      pointer != NULL;
      pointer = pointer->next) {
    printf("%d\n", pointer->data);
  }
  
}
